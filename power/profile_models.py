import csv
import json
import multiprocessing
import time
from timeit import default_timer as timer
import torch
from datasets import load_dataset
from transformers import PyTorchBenchmark, PyTorchBenchmarkArguments
from transformers import AutoImageProcessor, ResNetForImageClassification

import profiling.power_util as power_util


GPU_ID = 1
STATS_FILE_NAME = 'profiling_results.csv'

def get_stats():
    gpus = power_util.get_gpus()
    id = 0
    for gpu in gpus:
        if id < GPU_ID:
            id += 1
            continue
        info = json.loads(gpu.to_json())
        # print('ID:', info['id'], info['uuid'])
        gpu_util = round(info['gpu_util'], 3)
        mem_util = round(info['mem_util'], 3)
        # print('GPU Util:', str(gpu_util) + '%', 'Mem Util:', str(mem_util) + '% (' + str(info['mem_used']), 'MB in use)')
        power_watts = round(info['power'], 3)
        # print('Power:', power_watts, 'Watts')
        freq_mhz = int(info['freq'])
        # max_freq_mhz = int(info['max_freq'])
        # print('Frequency:', freq_mhz, 'MHz (max =', max_freq_mhz, 'MHz)')
        return gpu_util, mem_util, info['mem_used'], power_watts, freq_mhz

def collect_stats(stop_event, stats_queue):
    print('Started collecting stats...')
    stats_list = []
    while not stop_event.is_set():
        gpu_util, mem_util, mem_used_mb, power_watts, freq_mhz = get_stats()
        stats_list.append([gpu_util, mem_util, mem_used_mb, power_watts, freq_mhz])
        time.sleep(1)
    print('Stopped collecting stats!')
    stats_queue.put(stats_list)

def run_model(model_name, batch_size, sequence_length, result_queue):
    if model_name != 'resnet':
        args = PyTorchBenchmarkArguments(models=[model_name], batch_sizes=[batch_size], sequence_lengths=[sequence_length])
        benchmark = PyTorchBenchmark(args)
        results = benchmark.run()
        # print(results)
        result_queue.put(results)
        return results
    else:
        num_images = batch_size
        dataset = load_dataset("huggingface/cats-image")
        image = dataset["test"]["image"][0]

        processor = AutoImageProcessor.from_pretrained("microsoft/resnet-50")
        model = ResNetForImageClassification.from_pretrained("microsoft/resnet-50")

        processed_images = []
        for i in range(num_images):
            inputs = processor(image, return_tensors="pt")
            processed_images.append(inputs)

        output_logits = []
        start = timer()
        for inputs in processed_images:
            with torch.no_grad():
                logits = model(**inputs).logits
                output_logits.append(logits)
        end = timer()
        elapsed_time_ms = (end - start) * 1000

        # model predicts one of the 1000 ImageNet classes
        for logits in output_logits:
            predicted_label = logits.argmax(-1).item()
            print(model.config.id2label[predicted_label])

        # print('Latency =', elapsed_time_ms, 'ms')
        result_queue.put(elapsed_time_ms)
        return elapsed_time_ms


if __name__ == "__main__":
    # create a CSV file
    # header_fields = ['model', 'batch_size', 'sequence_length', 'record_id', 'freq_mhz', 'gpu_util', 'mem_util', 'mem_mb', 'power_watts', 'latency']
    # with open(STATS_FILE_NAME, mode='w') as f:
    #     writer = csv.writer(f)
    #     writer.writerow(header_fields)

    models = {
        'bert-base-uncased': {
            'batch_size': 32,
            'sequence_length': 512,
        },
        'bert-large-uncased': {
            'batch_size': 32,
            'sequence_length': 512,
        },
        'bigscience/bloom-560m': {
            'batch_size': 4,
            'sequence_length': 512,
        },
        'bigscience/bloom-1b1': {
            'batch_size': 4,
            'sequence_length': 512,
        },
        'bigscience/bloom-1b7': {
            'batch_size': 4,
            'sequence_length': 512,
        },
        'bigscience/bloom-3b': {
            'batch_size': 4,
            'sequence_length': 512,
        },
        'Salesforce/codegen-350M-mono': {
            'batch_size': 8,
            'sequence_length': 512,
        },
        'Salesforce/codegen-2B-mono': {
            'batch_size': 8,
            'sequence_length': 512,
        },
        'gpt2': {
            'batch_size': 8,
            'sequence_length': 512,
        },
        'gpt2-medium': {
            'batch_size': 8,
            'sequence_length': 512,
        },
        'gpt2-large': {
            'batch_size': 8,
            'sequence_length': 512,
        },
        'gpt2-xl': {
            'batch_size': 8,
            'sequence_length': 512,
        },
        'google/switch-base-8': {
            'batch_size': 16,
            'sequence_length': 512,
        },
        'google/switch-base-16': {
            'batch_size': 16,
            'sequence_length': 512,
        },
        'google/switch-base-32': {
            'batch_size': 16,
            'sequence_length': 512,
        },
        'facebook/opt-125m': {
            'batch_size': 8,
            'sequence_length': 512,
        },
        'facebook/opt-1.3b': {
            'batch_size': 8,
            'sequence_length': 512,
        },
        'facebook/opt-2.7b': {
            'batch_size': 8,
            'sequence_length': 512,
        },
        'roberta-base': {
            'batch_size': 32,
            'sequence_length': 512,
        },
        'roberta-large': {
            'batch_size': 32,
            'sequence_length': 512,
        },
        'resnet': {
            'batch_size': 5,
            'sequence_length': 512,
        }
    }

    for model_name in models:
        batch_size = models[model_name]['batch_size']
        sequence_length = models[model_name]['sequence_length']

        # start benchmarking a model and collect stats
        stop_event = multiprocessing.Event()

        result_queue = multiprocessing.Queue()
        stats_queue = multiprocessing.Queue()

        # start run_model in a separate process
        model_process = multiprocessing.Process(target=run_model, args=(model_name, batch_size, sequence_length,result_queue,))
        model_process.start()
        # start collect_stats in a separate process
        data_process = multiprocessing.Process(target=collect_stats, args=(stop_event,stats_queue,))
        data_process.start()

        # wait for model benchmarking to finish
        model_process.join()
        benchmarking_results = result_queue.get()
        if model_name == 'resnet':
            latency_ms = benchmarking_results
        else:
            latency_ms = round(benchmarking_results[0][model_name]['result'][batch_size][sequence_length] * 1000, 3)  # in ms
        stop_event.set()

        # wait for data collection to finish
        data_process.join()
        stats_list = stats_queue.get()
        stat_id = 0
        with open(STATS_FILE_NAME, mode='a') as file:
            for stats in stats_list:
                gpu_util, mem_util, mem_used_mb, power_watts, freq_mhz = stats
                csv_fields = [model_name, batch_size, sequence_length, stat_id, freq_mhz, gpu_util, mem_util, mem_used_mb, power_watts, latency_ms]
                writer = csv.writer(file)
                writer.writerow(csv_fields)
                stat_id += 1

        print('Model', model_name, 'benchmarking has finished!', '\n')