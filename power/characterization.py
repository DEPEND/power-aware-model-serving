import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib


nice_fonts = {
        # use LaTeX to write all text
        # "text.usetex": True,
        "font.family": "sans-serif", # "sans-serif",
        # use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 14,
        "font.size": 14,
        # make the legend/label fonts a little smaller
        "legend.fontsize": 12,
        "xtick.labelsize": 12,
        "ytick.labelsize": 12,
}

matplotlib.rcParams.update(nice_fonts)


df = pd.read_csv('profiling_results.csv')
print(df.head())

freq_list = list(range(200, 1301, 100))
models_abbr = {
    # 'resnet': 'resnet',
    'bert-base-uncased': 'bert-base',
    'bert-large-uncased': 'bert-large',
    'roberta-base': 'roberta-base',
    'roberta-large': 'roberta-large',
    'facebook/opt-1.3b': 'opt-1.3b',
    'facebook/opt-2.1b': 'opt-2.7b',
    'gpt2-large': 'gpt2-large',
    'gpt2-xl': 'gpt2-xl',
    'Salesforce/codegen-350M-mono': 'codegen-350m',
    # 'Salesforce/codegen-2B-mono': 'codegen-2b',
    'bigscience/bloom-1b1': 'bloom-1b1',
    'bigscience/bloom-3b': 'bloom-3b',
    'google/switch-base-16': 'switch-base-16',
    'google/switch-base-32': 'switch-base-32'
}

latency_dict = {}
power_dict = {}
for model in df['model'].unique().tolist():
    if model not in models_abbr:
        continue
    latency_list = []
    power_list = []
    for freq in freq_list:
        print(freq, model)
        if model == 'resnet':
            avg_latency = np.mean(df[(df['model']==model)&(abs(df['freq_mhz']-freq) < 50)]['latency'])
            avg_power = np.mean(df[(df['model']==model)&(abs(df['freq_mhz']-freq) < 50)]['power_watts'])
        else:
            avg_latency = np.mean(df[(df['model']==model)&(abs(df['freq_mhz']-freq) < 50)&(df['gpu_util']>0)&(df['mem_util']>0)]['latency'])
            avg_power = np.mean(df[(df['model']==model)&(abs(df['freq_mhz']-freq) < 50)&(df['gpu_util']>0)&(df['mem_util']>0)]['power_watts'])
        latency_list.append(avg_latency)
        power_list.append(avg_power)
    latency_dict[model] = latency_list
    power_dict[model] = power_list


fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(8, 4))
# fig = plt.figure(figsize=(9, 5))
# gs = gridspec.GridSpec(1, 2, height_ratios=[0.5])  # Adjust height ratios as needed
# ax1 = plt.subplot(gs[0])
# ax2 = plt.subplot(gs[1])
# ax = [ax1, ax2]

# plot latency vs. freq
for model in latency_dict:
    ax[0].plot(freq_list, latency_dict[model], '-x', label=models_abbr[model])
# plt.legend(ncols=2)
# plt.ylim(bottom=0)
# plt.xlabel('Frequency (MHz)')
# plt.ylabel('Latency (ms)')
ax[0].set_xlabel('Frequency (MHz)')
ax[0].set_ylabel('Latency (ms)')
# plt.tight_layout()
# plt.show()
ax[0].grid(True, axis='y', zorder=-1, linestyle='dashed', color='gray', alpha=0.5)

# plot power vs. freq
for model in power_dict:
    ax[1].plot(freq_list, power_dict[model], '-x', label=models_abbr[model])
# plt.legend(ncols=2)
# plt.ylim(bottom=0)
# plt.xlabel('Frequency (MHz)')
# plt.ylabel('Power (Watts)')
ax[1].set_xlabel('Frequency (MHz)')
ax[1].set_ylabel('Power (Watts)')
plt.grid(True, axis='y', zorder=-1, linestyle='dashed', color='gray', alpha=0.5)

handles, labels = ax[1].get_legend_handles_labels()
print(labels)
# fig.legend(handles, labels, loc='lower center', ncol=4, bbox_to_anchor=(0.55, -0.1), borderaxespad=0.)  # ,

fig.subplots_adjust(bottom=0.25)
fig.legend(handles, labels, loc='lower center', ncol=4, bbox_to_anchor=(0.54, 0), columnspacing=0.5)

plt.tight_layout(pad=5, w_pad=0.5, h_pad=0.5)
# plt.show()
plt.savefig('fig-2-power-curves.pdf')