import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib


nice_fonts = {
        # use LaTeX to write all text
        # "text.usetex": True,
        "font.family": "sans-serif", # "sans-serif",
        # use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 14,
        "font.size": 14,
        # make the legend/label fonts a little smaller
        "legend.fontsize": 12,
        "xtick.labelsize": 12,
        "ytick.labelsize": 12,
}

matplotlib.rcParams.update(nice_fonts)

total_time = 46  # total time in seconds
time_stamps = np.arange(total_time)
with open('arrival_rate.txt', 'r') as file:
    arrival_rate = [int(line.strip()) for line in file][:total_time]

print(arrival_rate)

# read the frequency from file
with open('frequency.txt', 'r') as file:
    frequency = [float(line.strip()) for line in file][:total_time]

fig, ax = plt.subplots(figsize=(6, 5))
ax2 = ax.twinx()

l1 = ax.plot(time_stamps, arrival_rate, '-', label='Rate', color='#FACE37')
l2 = ax2.plot(time_stamps, frequency, '--', label='Frequency', color='#3065AC', marker='x', markersize=3)

ax.set_xlabel('Time (s)')
ax.set_ylabel('Rate (req/s)', color='#FACE37')
ax2.set_ylabel('Frequency (MHz)', color='#3065AC')
ax2.set_ylim([200, 1400])

ax2.grid(True, axis='y', zorder=-1, linestyle='dashed', color='gray', alpha=0.5)

lns = l1 + l2
labs = [l.get_label() for l in lns]
ax.legend(lns, labs)
plt.tight_layout()
# plt.show()
plt.savefig('fig-13-scaling.pdf')