## Model Placement

### Usage

```
cd operator-clustering/run/
python placement.py
```

Note that the L370 needs to be configured to be the proper variable name for different experiments: `num_devices_list`, `slo_scales`, `insensitive_ops_percentage_list`

## Power-Perf Characterization of DL Models

### Usage

```
export GPU_NUM_DEVICES=1 PJRT_DEVICE=CUDA
export CUDA_VISIBLE_DEVICES=1
```

Start benchmarking and save profiling results to CSV file:

```
python profile_models.py
```

### GPU Frequency Tuning

To set the GPU frequency to a specific value, use the following command:

```
nvidia-smi --lock-gpu-clocks=<freq-in-mhz>
```