import csv
import numpy as np
import pandas as pd


df = pd.read_csv('profiling_results.csv')
print(df.head())

freq_list = list(range(200, 1301, 100))
models_abbr = {
    'resnet': 'resnet',
    'bert-base-uncased': 'bert-base',
    'bert-large-uncased': 'bert-large',
    'roberta-base': 'roberta-base',
    'roberta-large': 'roberta-large',
    'facebook/opt-1.3b': 'opt-1.3b',
    'facebook/opt-2.1b': 'opt-2.7b',
    'gpt2-large': 'gpt2-large',
    'gpt2-xl': 'gpt2-xl',
    'Salesforce/codegen-350M-mono': 'codegen-350m',
    'Salesforce/codegen-2B-mono': 'codegen-2b',
    'bigscience/bloom-1b1': 'bloom-1b1',
    'bigscience/bloom-3b': 'bloom-3b',
    'google/switch-base-16': 'switch-base-16',
    'google/switch-base-32': 'switch-base-32'
}

model_dict = {}
for model in df['model'].unique().tolist():
    if model not in models_abbr:
        continue
    latency_dict = {}
    for freq in freq_list:
        if model == 'resnet':
            avg_latency = np.mean(df[(df['model']==model)&(abs(df['freq_mhz']-freq) < 50)]['latency'])
        else:
            stable_memory = int(df[(df['model']==model)&(abs(df['freq_mhz']-freq) < 50)&(df['gpu_util']>0)&(df['mem_util']>0)]['mem_mb'].max())
            avg_latency = np.mean(df[(df['model']==model)&(abs(df['freq_mhz']-freq) < 50)&(df['gpu_util']>0)&(df['mem_util']>0)&(df['mem_mb']==stable_memory)]['latency'])
        latency_dict[freq] = round(avg_latency, 3)
    model_dict[model] = latency_dict
    print(model, latency_dict)

with open('model_profile.csv', 'w') as f:
    writer = csv.writer(f)
    writer.writerow(['model', 'freq_mhz', 'latency_ms'])
    for model, latency_dict in model_dict.items():
       for freq, latency in latency_dict.items():
           writer.writerow([model, freq, latency])