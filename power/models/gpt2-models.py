from transformers import AutoTokenizer
from transformers import PyTorchBenchmark, PyTorchBenchmarkArguments


model = "gpt2"
tokenizer = AutoTokenizer.from_pretrained(model)
print(tokenizer.model_max_length)  # 1024

# benchmarking
# batch_size = 8 -> 512 0.144s 0.404s 0.832s 1.602s
# batch_size = 16 -> 512 0.281s 0.791s 1.636s N/A
models = ["gpt2", "gpt2-medium", "gpt2-large", "gpt2-xl"]
args = PyTorchBenchmarkArguments(models=models, batch_sizes=[16], sequence_lengths=[128, 256, 384, 512])
benchmark = PyTorchBenchmark(args)
results = benchmark.run()
print(results)