from transformers import PyTorchBenchmark, PyTorchBenchmarkArguments


# benchmarking
# batch_size = 8 -> 512 0.382s
# batch_size = 16 -> 512 0.749s
# batch_size = 32 -> 512 1.371s
args = PyTorchBenchmarkArguments(models=["roberta-large"], batch_sizes=[32], sequence_lengths=[128, 256, 384, 512])
benchmark = PyTorchBenchmark(args)
results = benchmark.run()
print(results)