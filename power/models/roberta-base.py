from transformers import PyTorchBenchmark, PyTorchBenchmarkArguments


# benchmarking
# batch_size = 8 -> 512 0.135s
# batch_size = 16 -> 512 0.261s
# batch_size = 32 -> 512 0.512s
args = PyTorchBenchmarkArguments(models=["roberta-base"], batch_sizes=[32], sequence_lengths=[128, 256, 384, 512])
benchmark = PyTorchBenchmark(args)
results = benchmark.run()
print(results)