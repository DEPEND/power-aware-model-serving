from transformers import pipeline
from transformers import BertTokenizer, BertModel
from transformers import PyTorchBenchmark, PyTorchBenchmarkArguments


# benchmarking
# batch_size = 8 -> 512 0.365s
# batch_size = 16 -> 512 0.705s
# batch_size = 32 -> 512 1.302s
args = PyTorchBenchmarkArguments(models=["bert-large-uncased"], batch_sizes=[32], sequence_lengths=[128, 256, 384, 512])
benchmark = PyTorchBenchmark(args)
results = benchmark.run()
print(results)

# unmasking
# unmasker = pipeline('fill-mask', model='bert-large-uncased')
# result = unmasker("Hello I'm a [MASK] model.")
# print(result)

# encoding
# tokenizer = BertTokenizer.from_pretrained('bert-large-uncased')
# model = BertModel.from_pretrained("bert-large-uncased")
# text = "Replace me by any text you'd like."
# encoded_input = tokenizer(text, return_tensors='pt')
# output = model(**encoded_input)
# print(output)