from transformers import PyTorchBenchmark, PyTorchBenchmarkArguments


# benchmarking
# batch_size = 8 -> 512 0.123s
# batch_size = 16 -> 512 0.236s
# batch_size = 32 -> 512 0.462s
args = PyTorchBenchmarkArguments(models=["bert-base-uncased"], batch_sizes=[32], sequence_lengths=[128, 256, 384, 512])
benchmark = PyTorchBenchmark(args)
results = benchmark.run()
print(results)