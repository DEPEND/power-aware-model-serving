from transformers import AutoTokenizer
from transformers import PyTorchBenchmark, PyTorchBenchmarkArguments


model = "Salesforce/codegen-350M-mono"
tokenizer = AutoTokenizer.from_pretrained(model)
print(tokenizer.model_max_length)  # 2048

# benchmarking
# batch_size = 8 -> 512 0.357s 2.507s
models = ["Salesforce/codegen-350M-mono", "Salesforce/codegen-2B-mono"]
args = PyTorchBenchmarkArguments(models=models, batch_sizes=[8], sequence_lengths=[128, 256, 384, 512])
benchmark = PyTorchBenchmark(args)
results = benchmark.run()
print(results)