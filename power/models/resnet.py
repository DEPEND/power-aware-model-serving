from transformers import AutoImageProcessor, ResNetForImageClassification
import torch
from datasets import load_dataset
from timeit import default_timer as timer


# 5 images -> 253ms
num_images = 5
dataset = load_dataset("huggingface/cats-image")
image = dataset["test"]["image"][0]

processor = AutoImageProcessor.from_pretrained("microsoft/resnet-50")
model = ResNetForImageClassification.from_pretrained("microsoft/resnet-50")

processed_images = []
for i in range(num_images):
    inputs = processor(image, return_tensors="pt")
    processed_images.append(inputs)

output_logits = []
start = timer()
for inputs in processed_images:
    with torch.no_grad():
        logits = model(**inputs).logits
        output_logits.append(logits)
end = timer()
elapsed_time_ms = (end - start) * 1000

# model predicts one of the 1000 ImageNet classes
for logits in output_logits:
    predicted_label = logits.argmax(-1).item()
    print(model.config.id2label[predicted_label])

print('Latency =', elapsed_time_ms, 'ms')