from transformers import AutoTokenizer
from transformers import PyTorchBenchmark, PyTorchBenchmarkArguments


model = "gpt2"
tokenizer = AutoTokenizer.from_pretrained(model)
print(tokenizer.model_max_length)  # 1024

# benchmarking
# batch_size = 4 -> 512 0.298s 0.523s 0.79s 1.293s
# batch_size = 8 -> 512 0.571s 0.989s 1.498s N/A
models = ["bigscience/bloom-560m", "bigscience/bloom-1b1", "bigscience/bloom-1b7", "bigscience/bloom-3b"]
args = PyTorchBenchmarkArguments(models=models, batch_sizes=[4], sequence_lengths=[128, 256, 384, 512])
benchmark = PyTorchBenchmark(args)
results = benchmark.run()
print(results)