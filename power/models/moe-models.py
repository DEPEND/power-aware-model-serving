from transformers import AutoTokenizer
from transformers import PyTorchBenchmark, PyTorchBenchmarkArguments


model = "google/switch-base-8"
tokenizer = AutoTokenizer.from_pretrained(model)
print(tokenizer.model_max_length)  # 512

# benchmarking
# batch_size = 8 -> 512 0.316s 0.348s 0.402s
# batch_size = 16 -> 512 0.597s 0.635s 0.701s
models = ["google/switch-base-8", "google/switch-base-16", "google/switch-base-32"]
args = PyTorchBenchmarkArguments(models=["google/switch-base-32"], batch_sizes=[8], sequence_lengths=[128, 256, 384, 512])
benchmark = PyTorchBenchmark(args)
results = benchmark.run()
print(results)