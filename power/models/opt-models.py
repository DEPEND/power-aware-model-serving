from transformers import AutoTokenizer
from transformers import PyTorchBenchmark, PyTorchBenchmarkArguments


model = "facebook/opt-125m"
tokenizer = AutoTokenizer.from_pretrained(model)
print(tokenizer.model_max_length)

# benchmarking
# batch_size = 8 -> 512 0.134s 1.243s 2.351s
# batch_size = 16 -> 512 0.26s 2.464s N/A
models = ["facebook/opt-125m", "facebook/opt-1.3b", "facebook/opt-2.7b"]
args = PyTorchBenchmarkArguments(models=models, batch_sizes=[16], sequence_lengths=[128, 256, 384, 512])
benchmark = PyTorchBenchmark(args)
results = benchmark.run()
print(results)