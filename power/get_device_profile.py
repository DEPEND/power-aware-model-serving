import csv
import numpy as np
import pandas as pd


df = pd.read_csv('profiling_results.csv')
print(df.head())

freq_list = list(range(200, 1301, 100))
models_abbr = {
    # 'resnet': 'resnet',
    'bert-base-uncased': 'bert-base',
    'bert-large-uncased': 'bert-large',
    'roberta-base': 'roberta-base',
    'roberta-large': 'roberta-large',
    'facebook/opt-1.3b': 'opt-1.3b',
    'facebook/opt-2.1b': 'opt-2.7b',
    'gpt2-large': 'gpt2-large',
    'gpt2-xl': 'gpt2-xl',
    'Salesforce/codegen-350M-mono': 'codegen-350m',
    'Salesforce/codegen-2B-mono': 'codegen-2b',
    'bigscience/bloom-1b1': 'bloom-1b1',
    'bigscience/bloom-3b': 'bloom-3b',
    'google/switch-base-16': 'switch-base-16',
    'google/switch-base-32': 'switch-base-32'
}

power_dict = {}
for model in df['model'].unique().tolist():
    if model not in models_abbr:
        continue
    power_list = []
    for freq in freq_list:
        stable_memory = int(df[(df['model']==model)&(abs(df['freq_mhz']-freq) < 50)&(df['gpu_util']>0)&(df['mem_util']>0)]['mem_mb'].max())
        # if model == 'resnet':
        #     avg_power = np.mean(df[(df['model']==model)&(abs(df['freq_mhz']-freq) < 50)]['power_watts'])
        # else:
        #     avg_power = np.mean(df[(df['model']==model)&(abs(df['freq_mhz']-freq) < 50)&(df['gpu_util']>0)&(df['mem_util']>0)]['power_watts'])
        avg_power = np.mean(df[(df['model']==model)&(abs(df['freq_mhz']-freq) < 50)&(df['gpu_util']>0)&(df['mem_util']>0)&(df['mem_mb']==stable_memory)]['power_watts'])
        power_list.append(avg_power)
    power_dict[model] = power_list

power_profile = {}
for i in range(len(freq_list)):
    avg_power = round(np.mean([power_dict[model][i] for model in power_dict]), 3)
    power_profile[freq_list[i]] = avg_power

print('Device power profile:')
for freq in power_profile:
    print(freq, power_profile[freq], 'Watts')

with open('device_profile.csv', 'w') as f:
    writer = csv.writer(f)
    writer.writerow(['freq_mhz', 'power_watts'])
    for key, value in power_profile.items():
       writer.writerow([key, value])