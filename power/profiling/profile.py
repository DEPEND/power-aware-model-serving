import json
import time

import power_util

while True:
    gpus = power_util.get_gpus()
    for gpu in gpus:
        info = json.loads(gpu.to_json())
        print('ID:', info['id'], info['uuid'])
        gpu_util = round(info['gpu_util'], 3)
        mem_util = round(info['mem_util'], 3)
        print('GPU Util:', str(gpu_util) + '%', 'Mem Util:', str(mem_util) + '% (' + str(info['mem_used']), 'MB in use)')
        power_watts = round(info['power'], 3)
        print('Power:', power_watts, 'Watts')
        freq_mhz = int(info['freq'])
        max_freq_mhz = int(info['max_freq'])
        print('Frequency:', freq_mhz, 'MHz (max =', max_freq_mhz, 'MHz)')

    time.sleep(1)
    print()

# util.get_available_gpus()
# processes = util.get_gpu_processes()
# print(processes)
