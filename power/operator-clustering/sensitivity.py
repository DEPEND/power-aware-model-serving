import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib


nice_fonts = {
        # use LaTeX to write all text
        # "text.usetex": True,
        "font.family": "sans-serif", # "sans-serif",
        # use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 14,
        "font.size": 14,
        # make the legend/label fonts a little smaller
        "legend.fontsize": 14,
        "xtick.labelsize": 12,
        "ytick.labelsize": 12,
}

matplotlib.rcParams.update(nice_fonts)


ops = ['conv', 'fc', 'max_pool', 'avg_pool', 'bn', 'lrn', 'relu', 'sigmoid', 'softmax', 'tanh', 'lstm', 'gru', 'rnn', 'transpose', 'concat', 'encoder', 'decoder']
freqs = ['1500', '1800']

sensitivity_map = {}
latency_map = {}
scores = []
for op in ops:
    freq1 = freqs[0]
    freq2 = freqs[1]
    df = pd.read_csv('log/' + freq1 + '_' + op + '.csv')
    latency1 = np.mean(df['avg_time(ms)'])
    df = pd.read_csv('log/' + freq2 + '_' + op + '.csv')
    latency2 = np.mean(df['avg_time(ms)'])
    norm_latency1 = latency1 / latency2
    norm_freq2 = 1800 / 1500
    # sensitivity = round((latency1 - latency2) / (int(freq2) - int(freq1)), 3)
    # sensitivity = round((norm_latency1 - 1) / (int(freq2) - int(freq1)), 5)
    sensitivity = round((norm_latency1 - 1) / (norm_freq2 - 1), 5)
    print(op, 'sensivity score =', sensitivity, latency2, '->', latency1)
    sensitivity_map[op] = sensitivity
    latency_map[op] = [latency1]
    scores.append(sensitivity)

step=0.05
indices = np.arange(0, 1+step, step)
CDF = pd.DataFrame({'sample':scores})['sample'].quantile(indices)

# sns.kdeplot(data=scores, cumulative=True)
plt.figure(figsize=(6, 3))
plt.plot(CDF, indices, linewidth=3, color='blue')
plt.plot(0.15, 0.01, 'go', label='transpose')
plt.plot(0.28, 0.06, 'o', color='steelblue', label='softmax')
plt.plot(1.9, 0.33, 'yo', label='rsqrt')
plt.plot(3.1, 0.85, 'o', color='orange', label='dot')
plt.plot(4.5, 1, 'ro', label='conv')
plt.grid(color='gray', linestyle='--', linewidth=1, alpha=0.2)
plt.xlim(0)
plt.xlabel('Score')
plt.ylabel('CDF')
plt.legend()
plt.tight_layout()
plt.show()