from timeit import default_timer as timer
import time
import argparse
import shlex
from functools import reduce

import torch
import torch_xla
import torch_xla.core.xla_model as xm


SINGLE_CASE = True
LOGGING = False


class ConvInstance:
    def __init__(self, input_dim, filter_dim, stride):
        self.input_dim = input_dim
        self.filter_dim = filter_dim
        self.stride = [1, stride, stride, 1]

    def get_input_dim(self):
        return self.input_dim

    def get_filter_dim(self):
        return self.filter_dim

    def get_stride(self):
        return self.stride


class RnnInstance:
    def __init__(self, input_dim, num_units):
        self.input_dim = input_dim
        self.num_units = num_units

    def get_input_dim(self):
        return self.input_dim

    def get_num_units(self):
        return self.num_units


class FcInstance:
    def __init__(self, input_dim, num_outputs):
        self.input_dim = input_dim
        self.num_outputs = num_outputs

    def get_input_dim(self):
        return self.input_dim

    def get_num_outputs(self):
        return self.num_outputs


class PoolInstance:
    def __init__(self, input_dim, kernel_size, S_H, S_W):
        self.input_dim = input_dim 
        self.kernel_size = [1, kernel_size, kernel_size, 1]
        self.stride = [1, S_H, S_W, 1]

    def get_input_dim(self):
        return self.input_dim

    def get_kernel_size(self):
        return self.kernel_size

    def get_stride(self):
        return self.stride


class MaxPoolInstance(PoolInstance):
    def __init__(self, input_dim, kernel_size, S_H, S_W):
        PoolInstance.__init__(self, input_dim, kernel_size, S_H, S_W)


class AvgPoolInstance(PoolInstance):
    def __init__(self, input_dim, kernel_size, S_H, S_W):
        PoolInstance.__init__(self, input_dim, kernel_size, S_H, S_W)


class BnInstance:
    def __init__(self, input_dim, mean, variance, offset, scale, variance_epsilon):
        self.input_dim = input_dim
        self.mean = mean
        self.variance = variance
        self.offset = offset
        self.scale = scale
        self.variance_epsilon = variance_epsilon

    def get_mean(self):
        return self.mean

    def get_variance(self):
        return self.variance

    def get_offset(self):
        return self.offset

    def get_scale(self):
        return self.scale

    def get_variance_epsilon(self):
        return self.variance_epsilon

    def set_input_dim(self, input_dim):
        self.input_dim = input_dim

    def get_input_dim(self):
        return self.input_dim


class LrnInstance:
    def __init__(self, input_dim, depth_radius, bias, alpha, beta):
        self.input_dim = input_dim
        self.depth_radius = depth_radius
        self.bias = bias
        self.alpha = alpha
        self.beta = beta

    def get_depth_radius(self):
        return self.depth_radius

    def get_bias(self):
        return self.bias

    def get_alpha(self):
        return self.alpha

    def get_beta(self):
        return self.beta

    def get_input_dim(self):
        return self.input_dim

    def set_input_dim(self, input_dim):
        self.input_dim = input_dim


class ActiveInstance:
    def __init__(self, feature_dim):
        self.feature_dim = feature_dim

    def get_feature_dim(self):
        return self.feature_dim


class ReluInstance(ActiveInstance):
    def __init__(self, feature_dim):
        ActiveInstance.__init__(self, feature_dim)


class SigmoidInstance(ActiveInstance):
    def __init__(self, feature_dim):
        ActiveInstance.__init__(self, feature_dim)


class SoftmaxInstance(ActiveInstance):
    def __init__(self, feature_dim):
        ActiveInstance.__init__(self, feature_dim)


class TanhInstance(ActiveInstance):
    def __init__(self, feature_dim):
        ActiveInstance.__init__(self, feature_dim)


class TestCase():
    def __init__(self):
        self.test_case_list = []

    def convert_list(self, src_list):
        return list(map(int, src_list[1:-1].split(',')))


class RnnTestCase:
    def __init__(self):
        self.test_case_list = []
        self.vector_len = 64
                
    def gen_test_case(self, min_input_dim, max_input_dim, stride_input, min_units, max_units, stride_units):
        min_bs = min_input_dim[0]
        max_bs = max_input_dim[0]
        min_sl = min_input_dim[1]
        max_sl = max_input_dim[1]
        stride_bs = stride_input[0]
        stride_sl = stride_input[1]
        vl = self.vector_len

        for bs in range(min_bs, max_bs + 1, stride_bs):
            for sl in range(min_sl, max_sl + 1, stride_sl): 
                input_dim = [bs, sl, vl]
                for num_units in range(min_units, max_units + 1, stride_units): 
                    test_case = RnnInstance(input_dim, num_units)
                    self.test_case_list.append(test_case)

        return self.test_case_list


class ConvTestCase(TestCase):
    def __init__(self):
        TestCase.__init__(self)
        self.index = {'input_dim': 0, 'filter_dim': 1, 'stride': 2}

    def gen_test_case(self, param_file):
        with open(param_file) as lines:
            for line in lines:
                params = shlex.split(line)
                input_dim = params[self.index['input_dim']]
                filter_dim = params[self.index['filter_dim']]
                stride = int(params[self.index['stride']])
                input_dim = self.convert_list(input_dim)
                filter_dim = self.convert_list(filter_dim)
                test_case = ConvInstance(input_dim, filter_dim, stride)
                self.test_case_list.append(test_case)

        return self.test_case_list


class FcTestCase(TestCase):
    def __init__(self):
        TestCase.__init__(self)
        self.index = {'input_dim': 0, 'num_outputs': 1}

    def gen_test_case(self, param_file):
        input_dim_list = []
        with open(param_file) as lines:
            for line in lines:
                params = shlex.split(line)
                input_dim = params[self.index['input_dim']]
                input_dim = self.convert_list(input_dim)
                num_outputs = int(params[self.index['num_outputs']])
                input_dim.append(num_outputs)
                input_dim_list.append(input_dim)

        # sort input dimensions
        input_dim_list.sort(key=lambda dim: reduce(lambda x,y:x*y,dim,1))
        for input_dim in input_dim_list:
            test_case = FcInstance(input_dim[:-1], input_dim[-1])
            self.test_case_list.append(test_case)

        return self.test_case_list


class PoolTestCase(TestCase):
    def __init__(self):
        TestCase.__init__(self)
        self.index = {'input_dim': 0, 'kernel_size': 1, 'stride_h': 2, 'stride_w': 3}

    def gen_test_case(self, param_file):
        with open(param_file) as lines:
            for line in lines:
                params = shlex.split(line)
                input_dim = params[self.index['input_dim']]
                input_dim = self.convert_list(input_dim)
                kernel_size = int(params[self.index['kernel_size']])
                stride_h = int(params[self.index['stride_h']])
                stride_w = int(params[self.index['stride_w']])
                test_case = MaxPoolInstance(input_dim, kernel_size, stride_h, stride_w)
                self.test_case_list.append(test_case)

        return self.test_case_list


class BnTestCase(TestCase):
    def __init__(self):
        TestCase.__init__(self)
        self.index = {'input_dim': 0}

    def gen_test_case(self, param_file):
        input_dim_list= []
        with open(param_file) as lines:
            for line in lines:
                params = shlex.split(line)
                input_dim = params[self.index['input_dim']]
                input_dim = self.convert_list(input_dim)
                input_dim_list.append(input_dim)

        # sort input dimensions
        input_dim_list.sort(key=lambda dim: reduce(lambda x,y:x*y,dim,1))
        for input_dim in input_dim_list:
            test_case = BnInstance(input_dim, 0.35, 0.55, 2.3, 8.6, 0.03)
            self.test_case_list.append(test_case)

        return self.test_case_list


class LrnTestCase(TestCase):
    def __init__(self):
        TestCase.__init__(self)
        self.index = {'input_dim': 0}

    def gen_test_case(self, param_file):
        input_dim_list = []
        with open(param_file) as lines:
            for line in lines:
                params = shlex.split(line)
                input_dim = params[self.index['input_dim']]
                input_dim = self.convert_list(input_dim)
                input_dim_list.append(input_dim)

        # sort input dimensions
        input_dim_list.sort(key=lambda dim: reduce(lambda x,y:x*y,dim,1))
        for input_dim in input_dim_list:
            for depth_radius in range(1, 8):
                test_case = LrnInstance(input_dim, depth_radius, 1, 1e-4, 0.75)
                self.test_case_list.append(test_case)

        return self.test_case_list


class ActiveTestCase(TestCase):
    def __init__(self):
        TestCase.__init__(self)
        self.index = {'input_dim': 0}

    def gen_test_case(self, param_file):
        input_dim_list = []
        with open(param_file) as lines:
            for line in lines:
                params = shlex.split(line)
                input_dim = params[self.index['input_dim']]
                input_dim = self.convert_list(input_dim)
                input_dim_list.append(input_dim) 

        # sort input dimensions
        input_dim_list.sort(key=lambda dim: reduce(lambda x,y:x*y,dim,1))
        for input_dim in input_dim_list:
            test_case = ActiveInstance(input_dim)
            self.test_case_list.append(test_case)

        return self.test_case_list


class Ops:
    def __init__(self, times):
        self.times = times
        print(torch_xla.core.xla_model.xla_device_hw(xm.xla_device()))

    def conv(self, inst, log):
        print('conv op')
        # get input and filter dimensions and stride of current test instance
        input_dim = inst.get_input_dim()  # [N, H, W, C]
        filter_dim = inst.get_filter_dim()  # [filter_height, filter_width, in_channels, out_channels]
        stride = inst.get_stride()

        # create input data and weights
        op_input = torch.randn(input_dim[0], input_dim[3], input_dim[1], input_dim[2], device=xm.xla_device())

        # define op
        # torch.nn.Conv2d(in_channels, out_channels, kernel_size, stride=1)
        op = torch.nn.Conv2d(filter_dim[2], filter_dim[3], (filter_dim[0], filter_dim[1]), stride=(stride[1], stride[2])).to(xm.xla_device())

        # warm up op
        _ = op(op_input)

        # repeat op
        start = timer()
        for i in range(0, self.times):
            _ = op(op_input)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        # log record
        if LOGGING:
            log.write('%s,%s,%s,%.3f\n' %(input_dim, filter_dim, stride, avg))
            log.flush()

    def fc(self, inst, log):
        print('fc op')
        # get input dimensions and num_outputs of inst
        input_dim = inst.get_input_dim()
        num_outputs = inst.get_num_outputs()
        N = input_dim[0]
        D = int(reduce(lambda x,y:x*y, input_dim) / N)
        new_input_dim = [N, D]
        weight_dim = [D, num_outputs]

        # create input data and weights for inst
        op_data = torch.randn(input_dim, device=xm.xla_device())
        op_input = torch.reshape(op_data, new_input_dim)
        op_weights = torch.randn(weight_dim, device=xm.xla_device())

        # define op
        _ = torch.matmul(op_input, op_weights)

        # repeat op
        start = timer()
        for i in range(0, self.times):
            _ = torch.matmul(op_input, op_weights)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        # log record
        if LOGGING:
            log.write('%s,%s,%.3f\n' %(input_dim, num_outputs, avg))
            log.flush()

    def max_pool(self, inst, log):
        print('max_pool op')
        # get input dimensions, kernel size and stride
        input_dim = inst.get_input_dim()
        kernel_size = inst.get_kernel_size() 
        stride = inst.get_stride() 

        # create input data
        op_data = torch.randn(input_dim[0], input_dim[3], input_dim[1], input_dim[2], device=xm.xla_device())

        # define op
        # torch.nn.MaxPool2d(kernel_size, stride=None)
        op = torch.nn.MaxPool2d((kernel_size[2], kernel_size[3]), stride=(stride[2], stride[3])).to(xm.xla_device())
        # op = torch.nn.MaxPool2d((3, 2), stride=(2, 1)).to(xm.xla_device())
        # op_data = torch.randn(20, 16, 50, 32)

        # warm up op
        _ = op(op_data)
        
        # repeat op
        start = timer()
        for i in range(0, self.times):
            _ = op(op_data)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        # log record
        if LOGGING:
            log.write('%s,%s,%s,%.3f\n' %(input_dim, kernel_size, stride, avg))
            log.flush()

    def avg_pool(self, inst, log):
        print('avg_pool op')
        # get input dimensions, kernel size and stride
        input_dim = inst.get_input_dim()
        kernel_size = inst.get_kernel_size()
        stride = inst.get_stride()

        # create input data
        op_data = torch.randn(input_dim[0], input_dim[3], input_dim[1], input_dim[2], device=xm.xla_device())

        # define op
        # torch.nn.AvgPool2d(kernel_size, stride=None)
        op = torch.nn.AvgPool2d((kernel_size[2], kernel_size[3]), stride=(stride[2], stride[3])).to(xm.xla_device())

        # warm up op
        _ = op(op_data)

        # repeat op
        start = timer()
        for i in range(0, self.times):
            _ = op(op_data)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        # log record
        if LOGGING:
            log.write('%s,%s,%s,%.3f\n' %(input_dim, kernel_size, stride, avg))
            log.flush()

    def bn(self, inst, log):
        print('bn op')
        # get input dimensions and parameters
        input_dim = inst.get_input_dim()
        mean = inst.get_mean() 
        scale = inst.get_scale() 
        variance = inst.get_variance()
        offset = inst.get_offset() 
        variance_epsilon = inst.get_variance_epsilon()

        # create input data
        x = torch.rand(input_dim, device=xm.xla_device())

        # define op
        # torch.nn.BatchNorm2d(num_features, eps=1e-05, momentum=0.1, affine=True)
        op = torch.nn.BatchNorm2d(input_dim[1], eps=variance_epsilon, momentum=0.1, affine=True).to(xm.xla_device())
        # op = tf.nn.batch_normalization(x, mean, variance, offset, scale, variance_epsilon)

        # warm up op
        _ = op(x)

        # repeat op
        start = timer()
        for i in range(0, self.times):
            _ = op(x)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        # log record
        if LOGGING:
            log.write('%s,%.3f\n' %(input_dim, avg))
            log.flush()

    def lrn(self, inst, log):
        print('lrn op')
        # get input dimensions, depth radius, and parameters
        input_dim = inst.get_input_dim()
        depth_radius = inst.get_depth_radius() 
        alpha = inst.get_alpha() 
        bias = inst.get_bias() 
        beta = inst.get_beta()

        # create input data
        op_data = torch.randn(input_dim, device=xm.xla_device())

        # define op
        # torch.nn.LocalResponseNorm(size, alpha=0.0001, beta=0.75, k=1.0)
        op = torch.nn.LocalResponseNorm(depth_radius+1, alpha=alpha, beta=beta, k=bias).to(xm.xla_device())

        # warm up op
        _ = op(op_data)

        # repeat op
        start = timer()
        for i in range(0, self.times):
            _ = op(op_data)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        # log record
        if LOGGING:
            log.write('%s,%s,%.3f\n' %(input_dim, depth_radius, avg))
            log.flush()

    def relu(self, inst, log):
        print('relu op')
        # get input dimensions
        feature_dim = inst.get_feature_dim()

        # create input data
        features = torch.rand(feature_dim, device=xm.xla_device())

        # define op
        op = torch.nn.ReLU()

        # warm up op
        _ = op(features)

        # repeat op
        start = timer()
        for i in range(0, self.times):
            _ = op(features)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        # log record
        if LOGGING:
            log.write('%s,%.3f\n' %(feature_dim, avg))
            log.flush()

    def sigmoid(self, inst, log):
        print('sigmoid op')
        # get input dimensions
        feature_dim = inst.get_feature_dim() 

        # create input data
        x = torch.randn(feature_dim, device=xm.xla_device())
        
        # define op
        op = torch.nn.Sigmoid()

        # warm up op
        _ = op(x)

        # repeat op
        start = timer()
        for i in range(0, self.times):
            _ = op(x)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        # log record
        if LOGGING:
            log.write('%s,%.3f\n' %(feature_dim, avg))
            log.flush()

    def softmax(self, inst, log):
        print('softmax op')
        # get input dimensions
        feature_dim = inst.get_feature_dim()

        # create input data
        x = torch.randn(feature_dim, device=xm.xla_device())

        # define op
        op = torch.nn.Softmax(dim=1)

        # warm up op
        _ = op(x)

        # repeat op
        start = timer()
        for i in range(0, self.times):
            _ = op(x)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        # log record
        if LOGGING:
            log.write('%s,%.3f\n' %(feature_dim, avg))
            log.flush()

    def tanh(self, inst, log):
        print('tanh op')
        # get input dimensions
        feature_dim = inst.get_feature_dim()

        # create input data
        x = torch.randn(feature_dim, device=xm.xla_device())

        # define op
        op = torch.nn.Tanh()

        # warm up op
        _ = op(x)

        # repeat op
        start = timer()
        for i in range(0, self.times):
            _ = op(x)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        # log record
        if LOGGING:
            log.write('%s,%.3f\n' %(feature_dim, avg))
            log.flush()

    def lstm(self, inst, log):
        print('lstm op')
        # get input dimensions and units num
        input_dim = inst.get_input_dim()
        num_units = inst.get_num_units()

        # create input data
        inputs = torch.randn(input_dim, device=xm.xla_device())
        hx = torch.randn(input_dim[1], num_units, device=xm.xla_device())  # (batch, hidden_size)
        cx = torch.randn(input_dim[1], num_units, device=xm.xla_device())

        # define cell
        # torch.nn.LSTMCell(input_size, hidden_size, bias=True, device=None, dtype=None)
        cell = torch.nn.LSTMCell(input_dim[2], num_units).to(xm.xla_device())

        # warm up cell
        output = []
        for i in range(inputs.size()[0]):
            hx, cx = cell(inputs[i], (hx, cx))
            output.append(hx)
        # _ = cell(inputs)

        # repeat cell
        start = timer()
        for i in range(0, self.times):
            # _ = cell(inputs)
            output = []
            for i in range(inputs.size()[0]):
                hx, cx = cell(inputs[i], (hx, cx))
                output.append(hx)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        if LOGGING:
            log.write('%s,%s,%s,%s,%.3f\n' %(input_dim[0], input_dim[1], input_dim[2], num_units, avg))
            log.flush()
    
    def gru(self, inst, log):
        print('gru op')
        # get input dimensions and units num
        input_dim = inst.get_input_dim()
        num_units = inst.get_num_units()

        # create input data
        inputs = torch.randn(input_dim, device=xm.xla_device())
        hx = torch.randn(input_dim[1], num_units, device=xm.xla_device())  # (batch, hidden_size)

        # define cell
        cell = torch.nn.GRUCell(input_dim[2], num_units).to(xm.xla_device())

        # warm up cell
        # _ = cell(inputs)
        output = []
        for i in range(inputs.size()[0]):
            hx = cell(inputs[i], hx)
            output.append(hx)

        # repeat cell
        start = timer()
        for i in range(0, self.times):
            # _ = cell(inputs)
            output = []
            for i in range(inputs.size()[0]):
                hx = cell(inputs[i], hx)
                output.append(hx)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        if LOGGING:
            log.write('%s,%s,%s,%s,%.3f\n' %(input_dim[0], input_dim[1], input_dim[2], num_units, avg))
            log.flush()

    def rnn(self, inst, log):
        print('rnn op')
        # get input dimensions and units num
        input_dim = inst.get_input_dim()
        num_units = inst.get_num_units()

        # create input data
        inputs = torch.randn(input_dim, device=xm.xla_device())
        hx = torch.randn(input_dim[1], num_units, device=xm.xla_device())  # (batch, hidden_size)

        # define cell
        cell = torch.nn.RNNCell(input_dim[2], num_units).to(xm.xla_device())

        # warm up cell
        # _ = cell(inputs)
        output = []
        for i in range(inputs.size()[0]):
            hx = cell(inputs[i], hx)
            output.append(hx)

        # repeat cell
        start = timer()
        for i in range(0, self.times):
            output = []
            for i in range(inputs.size()[0]):
                hx = cell(inputs[i], hx)
                output.append(hx)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        if LOGGING:
            log.write('%s,%s,%s,%s,%.3f\n' %(input_dim[0], input_dim[1], input_dim[2], num_units, avg))
            log.flush()
    
    def transpose(self, inst, log):
        print('transpose op')
        # get input dimensions
        input_dim = inst.get_feature_dim()

        # create input data
        x = torch.rand(input_dim, device=xm.xla_device())

        # warm up op
        _ = torch.transpose(x, 0, 1)

        # repeat op
        start = timer()
        for i in range(0, self.times):
            _ = torch.transpose(x, 0, 1)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        # log record
        if LOGGING:
            log.write('%s,%.3f\n' %(input_dim, avg))
            log.flush()
    
    def cat(self, inst, log):
        print('cat op')
        # get input dimensions
        input_dim = inst.get_feature_dim()

        # create input data
        x = torch.rand(input_dim, device=xm.xla_device())

        # warm up op
        _ = torch.cat((x, x, x), 0)
        _ = torch.cat((x, x, x), 1)

        # repeat op
        start = timer()
        for i in range(0, self.times):
            _ = torch.cat((x, x, x), 0)
            _ = torch.cat((x, x, x), 1)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        # log record
        if LOGGING:
            log.write('%s,%.3f\n' %(input_dim, avg))
            log.flush()

    def encoder(self, inst, log):
        print('encoder op')
        # get input dimensions
        input_dim = inst.get_feature_dim()
        d_model = input_dim[0]
        nhead = input_dim[1]

        # create input data
        src = torch.rand(64, 128, d_model, device=xm.xla_device())

        # create op
        op = torch.nn.TransformerEncoderLayer(d_model=d_model, nhead=nhead).to(xm.xla_device())

        # warm up op
        _ = op(src)

        # repeat op
        start = timer()
        for i in range(0, self.times):
            _ = op(src)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        # log record
        if LOGGING:
            log.write('%s,%.3f\n' %(input_dim, avg))
            log.flush()

    def decoder(self, inst, log):
        print('decoder op')
        # get input dimensions
        input_dim = inst.get_feature_dim()
        d_model = input_dim[0]
        nhead = input_dim[1]

        # create input data
        memory = torch.rand(64, 128, d_model, device=xm.xla_device())
        tgt = torch.rand(64, 128, d_model, device=xm.xla_device())

        # create op
        op = torch.nn.TransformerDecoderLayer(d_model=d_model, nhead=nhead).to(xm.xla_device())

        # warm up op
        _ = op(memory, tgt)

        # repeat op
        start = timer()
        for i in range(0, self.times):
            _ = op(memory, tgt)
        end = timer()
        duration = (end - start) * 1000000
        avg = duration / self.times
        print("avg time(s): %.3f microsec" % avg, '| total:', duration)

        # log record
        if LOGGING:
            log.write('%s,%.3f\n' %(input_dim, avg))
            log.flush()


class OpTest():
    def __init__(self, op_name, times, interval):
        self.op_name = op_name
        self.op = Ops(times) 
        self.times = times
        self.interval = interval
        self.log = None
        if LOGGING:
            self.log = open('log/'+op_name+'.csv', 'w')

        # for RNN-related ops
        min_bs, max_bs = 16, 32
        stride_bs = 16
        min_sl, max_sl = 32, 64
        stride_sl = 32
        min_units, max_units = 256, 2560
        stride_units = 256
        self.min_input_dim = [min_bs, min_sl]
        self.max_input_dim = [max_bs, max_sl]
        self.stride_input = [stride_bs, stride_sl]
        self.min_units = min_units
        self.max_units = max_units
        self.stride_units = stride_units

    def run(self):
        if self.op_name == 'conv':
            self.test_conv()
        elif self.op_name == 'fc':
            self.test_fc()
        elif self.op_name == 'max_pool':
            self.test_max_pool()
        elif self.op_name == 'avg_pool':
            self.test_avg_pool()
        elif self.op_name == 'bn':
            self.test_bn()
        elif self.op_name == 'lrn':
            self.test_lrn()
        elif self.op_name == 'relu':
            self.test_relu()
        elif self.op_name == 'sigmoid':
            self.test_sigmoid()
        elif self.op_name == 'softmax':
            self.test_softmax()
        elif self.op_name == 'tanh':
            self.test_tanh()
        elif self.op_name == 'lstm':
            self.test_lstm()
        elif self.op_name == 'gru':
            self.test_gru()
        elif self.op_name == 'rnn':
            self.test_rnn()
        elif self.op_name == 'transpose':
            self.test_transpose()
        elif self.op_name == 'concat':
            self.test_cat()
        elif self.op_name == 'encoder':
            self.test_encoder()
        elif self.op_name == 'decoder':
            self.test_decoder()
        else:
            print('op not implemented!', op_name)

    def test_conv(self):
        generator = ConvTestCase()
        test_case_list = generator.gen_test_case('params/conv.params')

        if LOGGING:
            self.log.write('%s,%s,%s,%s\n' %('input_dim', 'filter_dim', 'stride', 'avg_time(ms)'))
            self.log.flush()
        for test_case in test_case_list:
            self.op.conv(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def test_fc(self):
        generator = FcTestCase()
        test_case_list = generator.gen_test_case('params/fc.params')

        if LOGGING:
            self.log.write('%s,%s,%s\n' %('input_dim', 'num_outputs', 'avg_time(ms)'))
            self.log.flush()
        for test_case in test_case_list:
            self.op.fc(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def test_max_pool(self):
        generator = PoolTestCase()
        test_case_list = generator.gen_test_case('params/pool.params')

        if LOGGING:
            self.log.write('%s,%s,%s,%s\n' %('input_dim', 'kernel_size', 'stride', 'avg_time(ms)'))
            self.log.flush()
        for test_case in test_case_list:
            self.op.max_pool(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def test_avg_pool(self):
        generator = PoolTestCase()
        test_case_list = generator.gen_test_case('params/pool.params')

        if LOGGING:
            self.log.write('%s,%s,%s,%s\n' %('input_dim', 'kernel_size', 'stride', 'avg_time(ms)'))
            self.log.flush()
        for test_case in test_case_list:
            self.op.avg_pool(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def test_bn(self):
        generator = BnTestCase()
        test_case_list = generator.gen_test_case('params/normal.params')

        if LOGGING:
            self.log.write('%s,%s\n' %('input_dim', 'avg_time(ms)'))
            self.log.flush()
        for test_case in test_case_list:
            self.op.bn(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def test_lrn(self):
        generator = LrnTestCase()
        test_case_list = generator.gen_test_case('params/normal.params')

        if LOGGING:
            self.log.write('%s,%s,%s\n' %('input_dim', 'depth_radius', 'avg_time(ms)'))
            self.log.flush()
        for test_case in test_case_list:
            self.op.lrn(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def test_relu(self):
        generator = ActiveTestCase()
        test_case_list = generator.gen_test_case('params/active.params')

        if LOGGING:
            self.log.write('%s,%s\n' %('input_dim', 'avg_time(ms)'))
            self.log.flush()
        for test_case in test_case_list:
            self.op.relu(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def test_sigmoid(self):
        generator = ActiveTestCase()
        test_case_list = generator.gen_test_case('params/active.params')

        if LOGGING:
            self.log.write('%s,%s\n' %('input_dim', 'avg_time(ms)'))
            self.log.flush()
        for test_case in test_case_list:
            self.op.sigmoid(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def test_softmax(self):
        generator = ActiveTestCase()
        test_case_list = generator.gen_test_case('params/active.params')

        if LOGGING:
            self.log.write('%s,%s\n' %('input_dim', 'avg_time(ms)'))
            self.log.flush()
        for test_case in test_case_list:
            self.op.softmax(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def test_tanh(self):
        generator = ActiveTestCase()
        test_case_list = generator.gen_test_case('params/active.params')

        if LOGGING:
            self.log.write('%s,%s\n' %('input_dim', 'avg_time(ms)'))
            self.log.flush()
        for test_case in test_case_list:
            self.op.tanh(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def test_lstm(self):
        generator = RnnTestCase()
        test_case_list = generator.gen_test_case(self.min_input_dim, self.max_input_dim, self.stride_input, self.min_units, self.max_units, self.stride_units)
        self.print_title()
        # iterate each test case
        for test_case in test_case_list:
            self.op.lstm(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def test_gru(self):
        generator = RnnTestCase()
        test_case_list = generator.gen_test_case(self.min_input_dim, self.max_input_dim, self.stride_input, self.min_units, self.max_units, self.stride_units)
        self.print_title()
        # iterate each test case
        for test_case in test_case_list:
            self.op.gru(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def test_rnn(self):
        generator = RnnTestCase()
        test_case_list = generator.gen_test_case(self.min_input_dim, self.max_input_dim, self.stride_input, self.min_units, self.max_units, self.stride_units)
        self.print_title()
        # iterate each test case
        for test_case in test_case_list:
            self.op.rnn(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def print_title(self):
        if LOGGING:
            self.log.write('%s,%s,%s,%s,%s\n' %('batch_size', 'seq_len', 'vector_len', 'hidden_units', 'avg_time(ms)'))
            self.log.flush()
    
    def test_transpose(self):
        generator = ActiveTestCase()
        test_case_list = generator.gen_test_case('params/matrix.params')

        if LOGGING:
            self.log.write('%s,%s\n' %('input_dim', 'avg_time(ms)'))
            self.log.flush()
        for test_case in test_case_list:
            self.op.transpose(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break
    
    def test_cat(self):
        generator = ActiveTestCase()
        test_case_list = generator.gen_test_case('params/matrix.params')

        if LOGGING:
            self.log.write('%s,%s\n' %('input_dim', 'avg_time(ms)'))
            self.log.flush()
        for test_case in test_case_list:
            self.op.cat(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def test_encoder(self):
        generator = ActiveTestCase()
        test_case_list = generator.gen_test_case('params/attention.params')

        if LOGGING:
            self.log.write('%s,%s\n' %('input_dim', 'avg_time(ms)'))
            self.log.flush()
        for test_case in test_case_list:
            self.op.encoder(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break

    def test_decoder(self):
        generator = ActiveTestCase()
        test_case_list = generator.gen_test_case('params/attention.params')

        if LOGGING:
            self.log.write('%s,%s\n' %('input_dim', 'avg_time(ms)'))
            self.log.flush()
        for test_case in test_case_list:
            self.op.decoder(test_case, self.log)
            time.sleep(self.interval)
            if SINGLE_CASE:
                break


def main(op_name, times, interval):
    op_test = OpTest(op_name, times, interval)
    op_test.run()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument('-op_name', default='fc', type=str, help='op name')
    parser.add_argument('-times', default='50', type=int, help='repeat times')
    parser.add_argument('-interval', default='0', type=int, help='time interval')
    args = parser.parse_args()
    op_name = args.op_name
    times = args.times
    interval = args.interval
    ops = ['conv', 'fc', 'max_pool', 'avg_pool', 'bn', 'lrn', 'relu', 'sigmoid', 'softmax', 'tanh', 'lstm', 'gru', 'rnn', 'transpose', 'concat', 'encoder', 'decoder']
    for op_name in ops:
        main(op_name, times, interval)
    # main(op_name, times, interval)