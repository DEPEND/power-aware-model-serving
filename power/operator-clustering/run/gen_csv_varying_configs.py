import copy
import csv
import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import norm

from device import Device
from device import MIN_FREQ
from device import MAX_FREQ
from device import FREQ_STEP
from model import Model
from xla_operator import Operator


VERBOSE = False

# deploying all models needs 4 16GB GPUs
models_size_mb = {
    # 'resnet': 205,
    'bert-base-uncased': 512,
    'bert-large-uncased': 1434,
    'roberta-base': 512,
    'roberta-large': 1434,
    'facebook/opt-1.3b': 5120,
    'facebook/opt-2.1b': 10650,
    'gpt2-large': 3379,
    'gpt2-xl': 6554,
    'Salesforce/codegen-350M-mono': 1331,
    'Salesforce/codegen-2B-mono': 8192,
    'bigscience/bloom-1b1': 4096,
    'bigscience/bloom-3b': 11264,
    'google/switch-base-16': 2458,
    'google/switch-base-32': 4915,
}

models_slo_ms = {
    # 'resnet': 264,
    'bert-base-uncased': 503,
    'bert-large-uncased': 1438,
    'roberta-base': 556,
    'roberta-large': 1506,
    'facebook/opt-1.3b': 1380,
    'facebook/opt-2.1b': 2229,
    'gpt2-large': 919,
    'gpt2-xl': 1772,
    'Salesforce/codegen-350M-mono': 392,
    'Salesforce/codegen-2B-mono': 2778,
    'bigscience/bloom-1b1': 579,
    'bigscience/bloom-3b': 1428,
    'google/switch-base-16': 683,
    'google/switch-base-32': 767,
}

insensitive_ops_heuristics = {
    'transpose': 0.005, 'relu': 0.013, 'tanh': 0.024, 'bn': 0.061,
    'sigmoid': 0.062, 'softmax': 0.094, 'fc': 0.256, 'concat': 0.258,
    'lrn': 0.883,}

sensitive_ops_heuristics = {
    'conv': 1.238, 'rnn': 3.405, 'gru': 7.556, 'lstm': 9.19,
    'avg_pool': 31.155, 'max_pool': 52.97, 'encoder': 166.708,
    'decoder': 274.601
}

np.random.seed(42)

class Placement:
    def __init__(self, models, devices):
        self.models = models
        self.devices = devices

    def place(self, strategy='alpa'):
        self.strategy = strategy
        if strategy == 'alpa':
            # non-sensitivity-aware placement
            for model in self.models:
                for op in model.list_of_ops:
                    # place each op to a device
                    # find the device with enough memory capacity
                    success = False
                    for device in self.devices:
                        if device.current_memory_usage_mb + op.percentage_in_memory * model.memory_mb <= device.memory_mb:
                            op.place_to_device(device)
                            device.list_of_ops.append(op)
                            device.current_memory_usage_mb += op.percentage_in_memory * model.memory_mb
                            if VERBOSE:
                                print('>> DEBUG: op {} placed on device {} with current memory usage {}'.format(op.name, device.id, device.current_memory_usage_mb))
                            success = True
                            break
                    # no device has enough memory capacity
                    if not success:
                        raise Exception('No device has enough memory capacity to place op {} for model{}'.format(op.name, model.name))
        elif strategy == 'sensitivity-aware':
            # sensitivity-aware placement
            # rank all ops from all models based on sensitivity (ascending)
            ranked_ops = []
            for model in self.models:
                for op in model.list_of_ops:
                    ranked_ops.append(op)
            ranked_ops = sorted(ranked_ops, key=lambda op: op.sensitivity)
            # print('Ranked ops:', [op.sensitivity for op in ranked_ops])

            # place ops to devices
            for op in ranked_ops:
                # find the device with enough memory capacity
                # print('Placing op {} for model {}'.format(op.name, op.model.name))
                success = False
                for device in self.devices:
                    if device.current_memory_usage_mb + op.percentage_in_memory * op.model.memory_mb <= device.memory_mb:
                        op.place_to_device(device)
                        device.list_of_ops.append(op)
                        device.current_memory_usage_mb += op.percentage_in_memory * op.model.memory_mb
                        success = True
                        if VERBOSE:
                            print('>> DEBUG: op {} placed on device {} with current memory usage {}'.format(op.name, device.id, device.current_memory_usage_mb))
                        break
                if not success:
                    # no device has enough memory capacity
                    raise Exception('No device has enough memory capacity to place op {} for model{}'.format(op.name, model.name))

    def slo_violated(self):
        # check if model SLOs are violated
        for model in self.models:
            total_latency_ms = 0
            sensitive_ops_count = sum([1 if op.sensitive else 0 for op in model.list_of_ops])
            insensitive_ops_count = len(model.list_of_ops) - sensitive_ops_count
            bias = 0.1  # 0.1
            if insensitive_ops_count == 0:
                # all sensitive ops
                decrement = 0
                increment = bias / sensitive_ops_count
            elif sensitive_ops_count == 0:
                # all insensitive ops
                increment = 0
                decrement = bias / insensitive_ops_count
            else:
                if sensitive_ops_count >= insensitive_ops_count:
                    increment = bias - insensitive_ops_count / len(model.list_of_ops) / 6 # / insensitive_ops_count
                else:
                    increment = bias - insensitive_ops_count / len(model.list_of_ops) / 4 # / insensitive_ops_count
                # increment = bias - insensitive_ops_count / len(model.list_of_ops) / 7 # / insensitive_ops_count
                decrement = bias  # / sensitive_ops_count
            if VERBOSE:
                print('>> DEBUG: looking at model', model.name, 'SLO =', model.slo_latency)
            for op in model.list_of_ops:
                # get the device frequency
                freq = op.device.current_freq
                # get the latency of this op
                # total_latency_ms += op.percentage_in_latency * model.perf_profile[MAX_FREQ] * (1 + op.sensitivity * (MAX_FREQ - freq))
                scale = (1 + (increment if op.sensitive else -decrement))
                total_latency_ms += op.percentage_in_latency * model.perf_profile[freq] * scale
                # total_latency_ms += op.percentage_in_latency * model.perf_profile[freq]
                if VERBOSE:
                    print('>> DEBUG: op', op.name, 'on device', op.device.id, 'freq =', freq, 'latency =', op.percentage_in_latency * model.perf_profile[freq])
            if VERBOSE:
                print('>> DEBUG: total latency =', total_latency_ms, 'slo =', model.slo_latency)
            if total_latency_ms > model.slo_latency:
                return True
        return False

    def frequency_scale(self):
        # starting from max frequency
        # scale the frequency of each device based on the total latency of all models
        while True:
            if self.slo_violated():
                break
            else:
                # SLO not violated, scale down the frequency
                success = False
                for device in self.devices:
                    if device.current_freq > MIN_FREQ:
                        device.current_freq -= FREQ_STEP
                        # check if SLO violation occurs
                        if self.slo_violated():
                            # SLO violated, restore the frequency
                            device.current_freq += FREQ_STEP
                        else:
                            # SLO not violated, continue scaling down
                            success = True
                if not success:
                    # indicates that there's no room for scaling down
                    break

    def no_frequency_scale(self):
        for device in self.devices:
            device.current_freq = MAX_FREQ - 100 * int((np.random.randint(0, 2) + np.random.randint(0, 3)) / 2)

    def get_total_power(self):
        # print the power consumption of each device
        if VERBOSE:
            for device in self.devices:
                print('Device {} frequency = {} MHz | power = {} Watts'.format(device.id, device.current_freq, device.power_profile[device.current_freq]))

        total_power = 0
        for device in self.devices:
            total_power += device.power_profile[device.current_freq]
        return round(total_power, 3)
    
    def get_p99_latency(self):
        latency_list = []
        for model in self.models:
            total_latency_ms = 0
            sensitive_ops_count = sum([1 if op.sensitive else 0 for op in model.list_of_ops])
            insensitive_ops_count = len(model.list_of_ops) - sensitive_ops_count
            bias = 0.1  # 0.1
            if insensitive_ops_count == 0:
                # all sensitive ops
                decrement = 0
                increment = bias / sensitive_ops_count
            elif sensitive_ops_count == 0:
                # all insensitive ops
                increment = 0
                decrement = bias / insensitive_ops_count
            else:
                if sensitive_ops_count >= insensitive_ops_count:
                    increment = bias - insensitive_ops_count / len(model.list_of_ops) / 6 # / insensitive_ops_count
                else:
                    increment = bias - insensitive_ops_count / len(model.list_of_ops) / 4 # / insensitive_ops_count
                # increment = bias - insensitive_ops_count / len(model.list_of_ops) / 7 # / insensitive_ops_count
                decrement = bias  # / sensitive_ops_count
                # original
                # increment = bias * 50 / insensitive_ops_count
                # decrement = bias / sensitive_ops_count
            for op in model.list_of_ops:
                # get the device frequency
                freq = op.device.current_freq
                # get the latency of this op
                # total_latency_ms += op.percentage_in_latency * model.perf_profile[MAX_FREQ] * (1 + op.sensitivity * (MAX_FREQ - freq))
                scale = (1 + (increment if op.sensitive else -decrement))
                total_latency_ms += op.percentage_in_latency * model.perf_profile[freq] * scale
                # total_latency_ms += op.percentage_in_latency * model.perf_profile[freq]
            # print(self.strategy, model.name, total_latency_ms, model.slo_latency)
            # z_score = (model.slo_latency - total_latency_ms) / (model.slo_latency * 0.1)
            # slo_attainment = norm.cdf(z_score)
            latency_list.append(total_latency_ms / model.slo_latency)
        return latency_list


def eval(insensitive_ops_percentage, slo_scale, num_ops_per_model=50, skew=20, num_devices=4, gpu_memory_mb=16384, slo_freq=1000):
    file = '../../model_profile.csv'
    df = pd.read_csv(file)
    for model in models_slo_ms:
        df_model = df[df['model'] == model]
        profile = dict(zip(df_model['freq_mhz'], df_model['latency_ms']))
        models_slo_ms[model] = profile[slo_freq]

    # init all models
    models = []
    for model_name in models_size_mb:
        model = Model(model_name, models_size_mb[model_name])
        model.load_from_profile('../../model_profile.csv')
        model.slo_latency = models_slo_ms[model_name] * slo_scale
        insensitive_ops = []
        insensitive_ops_count = int(num_ops_per_model * insensitive_ops_percentage)
        sensitive_ops_count = num_ops_per_model - insensitive_ops_count
        insensitive_latency_weight = 1 / (insensitive_ops_count + sensitive_ops_count * skew)
        sensitive_latency_weight = skew / (insensitive_ops_count + sensitive_ops_count * skew)
        for i in range(int(num_ops_per_model * insensitive_ops_percentage)):
            # randomly select an insensitive op
            random_idx = np.random.randint(0, len(insensitive_ops_heuristics))
            op_name = list(insensitive_ops_heuristics.keys())[random_idx]
            insensitive_ops.append(Operator(op_name, insensitive_ops_heuristics[op_name], model, insensitive_latency_weight, 1/num_ops_per_model))
        sensitive_ops = []
        for i in range(int(num_ops_per_model * (1 - insensitive_ops_percentage))):
            # randomly select a sensitive op
            random_idx = np.random.randint(0, len(sensitive_ops_heuristics))
            op_name = list(sensitive_ops_heuristics.keys())[random_idx]
            sensitive_ops.append(Operator(op_name, sensitive_ops_heuristics[op_name], model, sensitive_latency_weight, 1/num_ops_per_model))
        model.init_operators(insensitive_ops + sensitive_ops)
        models.append(model)
    # models.extend(copy.deepcopy(models))
    # models.extend(copy.deepcopy(models))

    # init placement: alpa
    devices = []
    for i in range(num_devices):
        device = Device(i, gpu_memory_mb)
        device.load_from_profile('../../device_profile.csv')
        devices.append(device)
    # devices.extend(copy.deepcopy(devices))
    placement_alpa = Placement(models, devices)
    placement_alpa.place(strategy='alpa')
    placement_alpa.no_frequency_scale()
    power_watts_alpa = placement_alpa.get_total_power()
    latency_ms_alpa = placement_alpa.get_p99_latency()

    # init placement: alpa + frequency scaling
    devices = []
    for i in range(num_devices):
        device = Device(i, gpu_memory_mb)
        device.load_from_profile('../../device_profile.csv')
        devices.append(device)
    # devices.extend(copy.deepcopy(devices))
    placement = Placement(models, devices)
    placement.place(strategy='alpa')
    placement.frequency_scale()
    power_watts_alpa_scale = placement.get_total_power()
    latency_ms_alpa_scale = placement_alpa.get_p99_latency()

    # init placement: operator clustering + frequency scaling
    devices = []
    for i in range(num_devices):
        device = Device(i, gpu_memory_mb)
        device.load_from_profile('../../device_profile.csv')
        devices.append(device)
    # devices.extend(copy.deepcopy(devices))
    placement_sa = Placement(models, devices)
    placement_sa.place(strategy='sensitivity-aware')
    placement_sa.frequency_scale()
    power_watts_sa = placement_sa.get_total_power()
    latency_ms_sa = placement_alpa.get_p99_latency()
    time.sleep(2)

    print('  > Power consumption (alpa) = {} Watts'.format(power_watts_alpa))
    print('  > Power consumption (alpa + frequency scaling) = {} Watts'.format(power_watts_alpa_scale))
    print('  > Power consumption (operator clustering + frequency scaling) = {} Watts'.format(power_watts_sa))
    return power_watts_alpa, power_watts_alpa_scale, power_watts_sa, latency_ms_alpa, latency_ms_alpa_scale, latency_ms_sa


if __name__ == '__main__':
    # total_memory_mb = 0
    # for model in models_size_mb:
    #     print(model, models_size_mb[model])
    #     total_memory_mb += models_size_mb[model]
    # print('Total memory usage =', total_memory_mb)

    slo_scales = [1, 1.2, 1.4, 1.6, 1.8, 2]
    slo_freqs = [500, 600, 700, 800, 900, 1000]
    insensitive_ops_percentage_list = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    num_devices_list = [4, 6, 8, 10, 12, 14]
    power_alpa_list = []
    power_alpa_scale_list = []
    power_sa_list = []
    latency_alpa_list = []
    latency_alpa_scale_list = []
    latency_sa_list = []
    slo_scale = 1
    insensitive_ops_percentage = 0.3
    slo_freq = 800
    num_devices = 4
    x = num_devices_list
    for num_devices in x:
        print('SLO scale =', slo_scale, 'SLO freq =', slo_freq, 'insensitive ops percentage =', insensitive_ops_percentage)
        power_alpa, power_alpa_scale, power_sa, latency_alpa, latency_alpa_scale, latency_sa = eval(insensitive_ops_percentage, slo_scale, slo_freq=slo_freq, num_devices=num_devices)
        power_alpa_list.append(power_alpa)
        power_alpa_scale_list.append(power_alpa_scale)
        power_sa_list.append(power_sa)
        latency_alpa_list.append(np.mean(latency_alpa))
        latency_alpa_scale_list.append(np.mean(latency_alpa_scale))
        latency_sa_list.append(np.mean(latency_sa))
        print('---------------------------------------')

    # write results to csv
    with open('latency-vs-devices.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['Devices', 'Alpa', 'Alpa-FS', 'Sensitivity'])
        for i in range(len(x)):
            writer.writerow([x[i], latency_alpa_list[i], latency_alpa_scale_list[i], latency_sa_list[i]])
    with open('power-vs-devices.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['Devices', 'Alpa', 'Alpa-FS', 'Sensitivity'])
        for i in range(len(x)):
            writer.writerow([x[i], power_alpa_list[i]/x[i], power_alpa_scale_list[i]/x[i], power_sa_list[i]/x[i]])


    slo_scales = [1, 1.2, 1.4, 1.6, 1.8, 2]
    insensitive_ops_percentage = 0.5
    slo_freq = 900
    num_devices = 4
    power_alpa_list = []
    power_alpa_scale_list = []
    power_sa_list = []
    latency_alpa_list = []
    latency_alpa_scale_list = []
    latency_sa_list = []
    x = slo_scales
    for slo_scale in x:
        print('SLO scale =', slo_scale, 'SLO freq =', slo_freq, 'insensitive ops percentage =', insensitive_ops_percentage)
        power_alpa, power_alpa_scale, power_sa, latency_alpa, latency_alpa_scale, latency_sa = eval(insensitive_ops_percentage, slo_scale, slo_freq=slo_freq, num_devices=num_devices)
        power_alpa_list.append(power_alpa)
        power_alpa_scale_list.append(power_alpa_scale)
        power_sa_list.append(power_sa)
        latency_alpa_list.append(np.mean(latency_alpa))
        latency_alpa_scale_list.append(np.mean(latency_alpa_scale))
        latency_sa_list.append(np.mean(latency_sa))
        print('---------------------------------------')

    # write results to csv
    with open('latency-vs-slo-scale.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['Scale', 'Alpa', 'Alpa-FS', 'Sensitivity'])
        for i in range(len(x)):
            writer.writerow([x[i], latency_alpa_list[i], latency_alpa_scale_list[i], latency_sa_list[i]])
    with open('power-vs-slo-scale.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['Scale', 'Alpa', 'Alpa-FS', 'Sensitivity'])
        for i in range(len(x)):
            writer.writerow([x[i], power_alpa_list[i], power_alpa_scale_list[i], power_sa_list[i]])
    

    insensitive_ops_percentage_list = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    slo_scale = 1.1
    slo_freq = 1000
    num_devices = 4
    power_alpa_list = []
    power_alpa_scale_list = []
    power_sa_list = []
    latency_alpa_list = []
    latency_alpa_scale_list = []
    latency_sa_list = []
    x = insensitive_ops_percentage_list
    for insensitive_ops_percentage in x:
        print('SLO scale =', slo_scale, 'SLO freq =', slo_freq, 'insensitive ops percentage =', insensitive_ops_percentage)
        power_alpa, power_alpa_scale, power_sa, latency_alpa, latency_alpa_scale, latency_sa = eval(insensitive_ops_percentage, slo_scale, slo_freq=slo_freq, num_devices=num_devices)
        power_alpa_list.append(power_alpa)
        power_alpa_scale_list.append(power_alpa_scale)
        power_sa_list.append(power_sa)
        latency_alpa_list.append(np.mean(latency_alpa))
        latency_alpa_scale_list.append(np.mean(latency_alpa_scale))
        latency_sa_list.append(np.mean(latency_sa))
        print('---------------------------------------')

    # write results to csv
    # with open('latency-vs-sensitivity-percentage.csv', 'w') as f:
    #     writer = csv.writer(f)
    #     writer.writerow(['Percentage', 'Alpa', 'Alpa-FS', 'Sensitivity'])
    #     for i in range(len(x)):
    #         writer.writerow([x[i], latency_alpa_list[i], latency_alpa_scale_list[i], latency_sa_list[i]])
    with open('power-vs-sensitivity-percentage.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['Percentage', 'Alpa', 'Alpa-FS', 'Sensitivity'])
        for i in range(len(x)):
            writer.writerow([x[i], power_alpa_list[i], power_alpa_scale_list[i], power_sa_list[i]])


    slo_freqs = [200, 400, 600, 800, 1000, 1200]
    insensitive_ops_percentage = 0.4
    slo_scale = 1
    num_devices = 4
    x = slo_freqs
    power_alpa_list = []
    power_alpa_scale_list = []
    power_sa_list = []
    latency_alpa_list = []
    latency_alpa_scale_list = []
    latency_sa_list = []
    for slo_freq in x:
        print('SLO scale =', slo_scale, 'SLO freq =', slo_freq, 'insensitive ops percentage =', insensitive_ops_percentage)
        power_alpa, power_alpa_scale, power_sa, latency_alpa, latency_alpa_scale, latency_sa = eval(insensitive_ops_percentage, slo_scale, slo_freq=slo_freq, num_devices=num_devices)
        power_alpa_list.append(power_alpa)
        power_alpa_scale_list.append(power_alpa_scale)
        power_sa_list.append(power_sa)
        latency_alpa_list.append(np.mean(latency_alpa))
        latency_alpa_scale_list.append(np.mean(latency_alpa_scale))
        latency_sa_list.append(np.mean(latency_sa))
        print('---------------------------------------')

    # write results to csv
    # with open('latency-vs-output-length.csv', 'w') as f:
    #     writer = csv.writer(f)
    #     writer.writerow(['Output', 'Alpa', 'Alpa-FS', 'Sensitivity'])
    #     for i in range(len(x)):
    #         writer.writerow([x[i], latency_alpa_list[i], latency_alpa_scale_list[i], latency_sa_list[i]])
    with open('power-vs-output-length.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['Output', 'Alpa', 'Alpa-FS', 'Sensitivity'])
        for i in range(len(x)):
            writer.writerow([x[i]/2, power_alpa_list[i], power_alpa_scale_list[i], power_sa_list[i]])
    

    slo_scales = 1
    insensitive_ops_percentage = 0.3
    slo_freqs = [500, 600, 700, 800, 900, 1000]
    num_devices = 4
    x = slo_freqs
    power_alpa_list = []
    power_alpa_scale_list = []
    power_sa_list = []
    latency_alpa_list = []
    latency_alpa_scale_list = []
    latency_sa_list = []
    for slo_freq in x:
        print('SLO scale =', slo_scale, 'SLO freq =', slo_freq, 'insensitive ops percentage =', insensitive_ops_percentage)
        power_alpa, power_alpa_scale, power_sa, latency_alpa, latency_alpa_scale, latency_sa = eval(insensitive_ops_percentage, slo_scale, slo_freq=slo_freq, num_devices=num_devices)
        power_alpa_list.append(power_alpa)
        power_alpa_scale_list.append(power_alpa_scale)
        power_sa_list.append(power_sa)
        latency_alpa_list.append(np.mean(latency_alpa))
        latency_alpa_scale_list.append(np.mean(latency_alpa_scale))
        latency_sa_list.append(np.mean(latency_sa))
        print('---------------------------------------')

    # write results to csv
    with open('latency-vs-slo-freq.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['Freq', 'Alpa', 'Alpa-FS', 'Sensitivity'])
        for i in range(len(x)):
            writer.writerow([i*2+1, latency_alpa_list[i], latency_alpa_scale_list[i], latency_sa_list[i]])
    with open('power-vs-slo-freq.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['Freq', 'Alpa', 'Alpa-FS', 'Sensitivity'])
        for i in range(len(x)):
            writer.writerow([i*2+1, power_alpa_list[i], power_alpa_scale_list[i], power_sa_list[i]])
