import pandas as pd
import numpy as np


MIN_FREQ = 200
MAX_FREQ = 1300
FREQ_STEP = 100

class Device:
    def __init__(self, id, memory_mb, power_profile=None):
        self.id = id
        self.memory_mb = memory_mb  # capacity
        self.power_profile = power_profile  # a dict of <frequency, power> pairs
        self.current_freq = MAX_FREQ  # MHz
        self.current_power = 0  # Watt
        self.list_of_ops = []  # list of ops placed on this device
        self.current_memory_usage_mb = 0

    def load_from_profile(self, csv_file):
        self.power_profile = {}
        df = pd.read_csv(csv_file)
        for freq in list(df['freq_mhz'].unique()):
            self.power_profile[freq] = round(np.mean(df[df['freq_mhz'] == freq]['power_watts']), 3)


if __name__ == '__main__':
    model = Device(0, 16*1024)
    model.load_from_profile('../../device_profile.csv')
    print(model.id, model.memory_mb)
    print(model.power_profile)
