import pandas as pd


class Model:
    def __init__(self, name, memory_mb, slo_latency=0, perf_profile=None):
        self.name = name
        self.memory_mb = memory_mb
        self.perf_profile = perf_profile  # a dict of <frequency, latency> pairs
        self.list_of_ops = []
        self.slo_latency = slo_latency

    def load_from_profile(self, csv_file):
        df = pd.read_csv(csv_file)
        if self.name not in df['model'].values:
            raise Exception('Model {} not found in profile'.format(self.name))
        df_model = df[df['model'] == self.name]
        self.perf_profile = dict(zip(df_model['freq_mhz'], df_model['latency_ms']))

    def init_operators(self, operators):
        self.list_of_ops = operators


if __name__ == '__main__':
    model = Model('resnet', 1024)
    model.load_from_profile('../../model_profile.csv')
    print(model.name, model.memory_mb)
    print(model.perf_profile)

    file = '../../model_profile.csv'
    df = pd.read_csv(file)
    for model in list(df['model'].unique()):
        df_model = df[df['model'] == model]
        profile = dict(zip(df_model['freq_mhz'], df_model['latency_ms']))
        print(model, profile[700], profile[1300])