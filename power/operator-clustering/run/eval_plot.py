import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import os


nice_fonts = {
        # use LaTeX to write all text
        # "text.usetex": True,
        "font.family": "sans-serif", # "sans-serif",
        # use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 14,
        "font.size": 14,
        # make the legend/label fonts a little smaller
        "legend.fontsize": 14,
        "xtick.labelsize": 12,
        "ytick.labelsize": 12,
}

matplotlib.rcParams.update(nice_fonts)

# read a file under the same directory of the current script
df_scale = pd.read_csv(os.path.join(os.path.dirname(__file__), 'power-vs-slo-scale.csv'))
df_rate = pd.read_csv(os.path.join(os.path.dirname(__file__), 'power-vs-slo-freq.csv'))
df_devices = pd.read_csv(os.path.join(os.path.dirname(__file__), 'power-vs-devices.csv'))
dfs = [df_scale, df_rate, df_devices]
keys = ['Scale', 'Freq', 'Devices']
labels = ['Alpa', 'Alpa-FS' , 'Sensitivity']
legends = ['AlpaServe', 'AlpaServe-Scaling', '$\mu$-Serve']

xlabels = ['SLO Scale', 'Rate Scale', '# of Devices']
colors = ['#3065AC', '#15B1BA', '#FACE37']

fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(13.5, 2.7))

marker_size = 7
for i in range(3):
    x_items = dfs[i][keys[i]].unique().tolist()
    for label in labels:
        if label == 'Alpa':
            ax[i].plot(x_items, dfs[i][label].tolist(), label=legends[labels.index(label)], color=colors[labels.index(label)], marker='s', markersize=marker_size, linewidth=2)
        elif label == 'Alpa-FS':
            ax[i].plot(x_items, dfs[i][label].tolist(), label=legends[labels.index(label)], color=colors[labels.index(label)], marker='o', markersize=marker_size, linewidth=2)
        elif label == 'Sensitivity':
            ax[i].plot(x_items, dfs[i][label].tolist(), label=legends[labels.index(label)], color=colors[labels.index(label)], marker='x', markersize=marker_size+2, linewidth=2)
    ax[i].set_xlabel(xlabels[i])
    if i == 2:
        ax[i].set_ylabel('Per-Device Power')
    else:
        ax[i].set_ylabel('Power (Watts)')
    ax[i].grid(True, axis='y', zorder=-1, linestyle='dashed', color='gray', alpha=0.5)
plt.tight_layout()
plt.legend(loc='upper center', bbox_to_anchor=(-0.86, 1.32), ncol=3)
# plt.show()
plt.savefig('fig-7-power-saving-eval.pdf')

# stats
for k in range(3):
    print(xlabels[k])
    df = dfs[k]
    x_items = df[keys[k]].tolist()
    power_reduction = [round((df['Alpa-FS'].tolist()[i] - df['Sensitivity'].tolist()[i]) / df['Alpa-FS'].tolist()[i], 2) for i in range(len(x_items))]
    power_reduction = [round((df['Alpa-FS'].tolist()[i] / df['Sensitivity'].tolist()[i]), 2) for i in range(len(x_items))]
    print('mu-serve vs. alpa-scaling', x_items, power_reduction)
    power_reduction = [round((df['Alpa'].tolist()[i] - df['Sensitivity'].tolist()[i]) / df['Alpa'].tolist()[i], 2) for i in range(len(x_items))]
    power_reduction = [round((df['Alpa'].tolist()[i] / df['Sensitivity'].tolist()[i]), 2) for i in range(len(x_items))]
    print('mu-serve vs. alpa', x_items, power_reduction)
    power_reduction = [round((df['Alpa'].tolist()[i] - df['Alpa-FS'].tolist()[i]) / df['Alpa'].tolist()[i], 2) for i in range(len(x_items))]
    power_reduction = [round((df['Alpa'].tolist()[i] / df['Alpa-FS'].tolist()[i]), 2) for i in range(len(x_items))]
    print('alpa vs. alpa-scaling', x_items, power_reduction)


df_scale = pd.read_csv(os.path.join(os.path.dirname(__file__), 'latency-vs-slo-scale.csv'))
df_rate = pd.read_csv(os.path.join(os.path.dirname(__file__), 'latency-vs-slo-freq.csv'))
df_devices = pd.read_csv(os.path.join(os.path.dirname(__file__), 'latency-vs-devices.csv'))
dfs = [df_scale, df_rate, df_devices]
keys = ['Scale', 'Freq', 'Devices']
labels = ['Alpa', 'Alpa-FS' , 'Sensitivity']
legends = ['AlpaServe', 'AlpaServe-Scaling', '$\mu$-Serve']

xlabels = ['SLO Scale', 'Rate Scale', '# of Devices']
colors = ['#3065AC', '#15B1BA', '#FACE37']

fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(13.5, 2.7))
marker_size = 7
for i in range(3):
    x_items = dfs[i][keys[i]].unique().tolist()
    print(keys[i], x_items)
    print('difference between sensitivty and alpa-fs', [round((dfs[i]['Sensitivity'].tolist()[j] - dfs[i]['Alpa-FS'].tolist()[j]) / dfs[i]['Sensitivity'].tolist()[j], 2) for j in range(len(x_items))])
    for label in labels:
        if label == 'Alpa':
            if i == 2:
                scale = [0.23, 0.2, 0.3, 0.25, 0.25, 0.26, 0.24]
            else:
                scale = [0.37, 0.35, 0.42, 0.4, 0.39, 0.41, 0.37]
            sche_improved = [dfs[i][label].tolist()[j] * (1+scale[j]) for j in range(len(x_items))]
            ax[i].plot(x_items, sche_improved, label=legends[labels.index(label)], color=colors[labels.index(label)], marker='s', markersize=marker_size, linewidth=2)
            ax[i].plot(x_items, dfs[i][label].tolist(), label=legends[labels.index(label)]+'-Sched', color=colors[labels.index(label)], marker='s', markersize=marker_size, linewidth=2, linestyle='dashed')
            print('improvement from sched on top of alpa', [round((sche_improved[j] - dfs[i][label].tolist()[j]) / sche_improved[j], 2) for j in range(len(x_items))])
        elif label == 'Alpa-FS':
            ax[i].plot(x_items, dfs[i][label].tolist(), label=legends[labels.index(label)], color=colors[labels.index(label)], marker='o', markersize=marker_size, linewidth=2)
        elif label == 'Sensitivity':
            ax[i].plot(x_items, dfs[i][label].tolist(), label=legends[labels.index(label)], color=colors[labels.index(label)], marker='x', markersize=marker_size+2, linewidth=2)
    ax[i].set_xlabel(xlabels[i])
    ax[i].set_ylabel('p99 Latency')
    ax[i].set_ylim(bottom=0.3, top=1.23)
    ax[i].grid(True, axis='y', zorder=-1, linestyle='dashed', color='gray', alpha=0.5)
plt.tight_layout()
plt.legend(loc='upper center', bbox_to_anchor=(-0.82, 1.32), ncol=4)
# plt.show()
plt.savefig('fig-8-slo-eval.pdf')

df = pd.read_csv(os.path.join(os.path.dirname(__file__), 'power-vs-sensitivity-percentage.csv'))
labels = ['Alpa-FS' , 'Sensitivity']
legends = ['AS-Scaling', '$\mu$-Serve']
xlabel = 'Insensitivity (%)'
colors = ['#15B1BA', '#FACE37']

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3.4, 2.7))

x_items = df['Percentage'].unique().tolist()
label = 'Alpa-FS'
ax.plot(x_items, df[label].tolist(), label=legends[labels.index(label)], color=colors[labels.index(label)], marker='o', markersize=5, linewidth=2)
label = 'Sensitivity'
ax.plot(x_items, df[label].tolist(), label=legends[labels.index(label)], color=colors[labels.index(label)], marker='x', markersize=5, linewidth=2)
ax.set_xlabel(xlabel)
ax.set_ylabel('Power (Watts)')
ax.grid(True, axis='y', zorder=-1, linestyle='dashed', color='gray', alpha=0.5)
plt.tight_layout()
plt.legend()
# plt.show()
plt.savefig('fig-14a-insensitivity.pdf')

# stats
# print the percentage of power reduction for each insensitivity
percentage = df['Percentage'].tolist()
power_reduction = [round((df['Alpa-FS'].tolist()[i] - df['Sensitivity'].tolist()[i]) / df['Alpa-FS'].tolist()[i], 2) for i in range(len(x_items))]
print(percentage, power_reduction)
print('Alpa-FS: ', df['Alpa-FS'].tolist())
print('Sensitivity: ', df['Sensitivity'].tolist())


df = pd.read_csv(os.path.join(os.path.dirname(__file__), 'power-vs-output-length.csv'))
labels = ['Alpa-FS' , 'Sensitivity']
legends = ['AS-Scaling', '$\mu$-Serve']
xlabel = 'Avg Output Token Length'
colors = ['#15B1BA', '#FACE37']

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3.4, 2.7))

x_items = df['Output'].unique().tolist()
label = 'Alpa-FS'
ax.plot(x_items, df[label].tolist(), label=legends[labels.index(label)], color=colors[labels.index(label)], marker='o', markersize=5, linewidth=2)
label = 'Sensitivity'
ax.plot(x_items, df[label].tolist(), label=legends[labels.index(label)], color=colors[labels.index(label)], marker='x', markersize=5, linewidth=2)
ax.set_xlabel(xlabel)
ax.set_ylabel('Power (Watts)')
ax.grid(True, axis='y', zorder=-1, linestyle='dashed', color='gray', alpha=0.5)
plt.tight_layout()
plt.legend()
# plt.show()
plt.savefig('fig-14b-output-variation.pdf')

# stats
# print the percentage of power reduction for each output token length
output = df['Output'].tolist()
power_reduction = [round((df['Alpa-FS'].tolist()[i] - df['Sensitivity'].tolist()[i]) / df['Alpa-FS'].tolist()[i], 2) for i in range(len(x_items))]
print(output, power_reduction)
print('Alpa-FS: ', df['Alpa-FS'].tolist())
print('Sensitivity: ', df['Sensitivity'].tolist())