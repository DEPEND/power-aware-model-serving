import pandas as pd
import numpy as np


class Operator:
    def __init__(self, name, sensitivity, model, percentage_in_latency, percentage_in_memory, device=None):
        self.name = name
        self.sensitivity = sensitivity
        self.model = model
        self.percentage_in_latency = percentage_in_latency
        self.percentage_in_memory = percentage_in_memory
        self.device = device
        if self.sensitivity > 1:
            self.sensitive = True
        else:
            self.sensitive = False

    def load_from_profile(self, csv_file):
        df = pd.read_csv(csv_file)
        if self.name not in df['op'].values:
            raise Exception('Operator {} not found in profile'.format(self.name))
        df_op = df[df['op'] == self.name]
        self.sensitivity = round(np.mean(df_op['sensitivity']), 3)

    def place_to_device(self, device):
        self.device = device


if __name__ == '__main__':
    op = Operator('conv', 0, 'bert', 20, 30)
    op.load_from_profile('../profile.csv')
    print(op.name, op.sensitivity)

    file = '../profile.csv'
    df = pd.read_csv(file)
    ops = {}
    for op in list(df['op'].unique()):
        df_op = df[df['op'] == op]
        print(op, round(np.mean(df_op['sensitivity']), 3))
        ops[op] = round(np.mean(df_op['sensitivity']), 3)
    # sort ops based on value
    ops = {k: v for k, v in sorted(ops.items(), key=lambda item: item[1])}
    print(ops)