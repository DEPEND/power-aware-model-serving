#!/bin/bash

for n in {200..1200..200}; 
do
    echo $n
    nvidia-smi --lock-gpu-clocks=$n
    python profile_models.py
done