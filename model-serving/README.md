# SSJF Scheduler

The key idea/observation is that a small *proxy model* (i.e., a fine-tuned BERT-base model) can predict the LLM verbosity well given the input query.
Based on such a *proxy model*, we present the design and implementation of a speculative shortest-job-first (SSJF) scheduler for LLM serving, using a proxy-model-based sequence length predictor for execution time estimation.
SSJF can be directly applied (1) in existing LLM serving systems with no need to change the memory or key-value cache management, and (2) in various batching settings, i.e., no batching, dynamic batching, and continuous (iteration) batching (e.g., in Orca/vLLM).

Below is the overall architecture of the proposed LLM-serving system:

<p align="center">
  <img src="../docs/scheduler.png" width="450" title="architecture">
</p>

Input requests from users are dispatched to corresponding models at runtime.
For each model instance, a request queue is maintained, and SSJF runs a speculative shortest-job-first scheduler to decide the request execution order.
SJF alleviates the non-determinism of generative models and the head-of-line blocking issue in FCFS scheduling.
Since we do not have the ground truth execution time of each request (required by SJF), SSJF relies on the prediction from an output token length predictor.
The prediction is used to estimate the job execution time in SJF as the output token length dominates the execution time (linear relation).
The predictor relies on a lightweight proxy model, i.e., a BERT-base model in our case.

The architecture of the proxy model is shown below:

<p align="center">
  <img src="../docs/predictor.png" width="450" title="architecture">
</p>

We formulate the output token length prediction for each query as a regression problem (option #1) or a multi-class classification problem (option #2), as shown in (a).
In the regression task, the prediction is the absolute output token length while in classification, the prediction is a category of percentile values.
For example, in binary-class classification, two categories could be `[0, p50)` and `[p50, max]`, where `p50` is the median output token length based on historical LLM serving records.

An alternative formulation is that we can have a pairwise predictor, as shown in (b), whose input are two LLM queries (Q1 and Q2) and the prediction is 1 if Q1’s output is longer than Q2’s and 0 otherwise.
Given such a pairwise predictor, the SJF scheduler can insert any new arrival request to the sorted queue (based on output length) based on the pairwise prediction of the new request and any existing requests in the queue. However, evaluation results show that SJF with pairwise prediction has only ~3% improvement in JCT compared to FCFS (SSJF has >10× higher improvement).
Therefore, we do not proceed with this approach.

## Scheduling Simulator

Scheduling simulator is based on runtime data and prediction results located at `prediction/final/`.

### Usage

```
$ python3 simulator.py [-h] -n NUM_JOBS -a ALGO -d DISTRIBUTION [-r RATE] [-s STD] [-c COEFFICIENT_OF_VARIATION] [--llm] [-m MODEL] [--per_token_latency LATENCY] [--const_latency LATENCY]
```

Required Arguments:
- `-n`: Number of jobs to create
- `-a`: Scheduling algorithm, chosen from FCFS and SJF
- `-d`: Job arrival distribution, chosen from Poisson, Uniform, and Gamma

Optional Arguments:
- `-r`: Average job arrival rate, default 1
- `-s`: Standard deviation of job arrival rate, used for Unifrom distribution, default 1
- `-c`: Coefficient of variance of job arrival rate, used for Gamma distribution, default 2
- `--llm`: Create jobs for LLM
- `-m`: LLM model
- `--per_token_latency`: Average per token generation latency for the LLM model, default 0.02
- `--const_latency`: Constant latency for token generation, amortized across all generated tokens for the LLM model, default 0.1

Batching-related parameters:

```
MAX_BATCH_SIZE = 1
BATCH_WAIT_TIMEOUT = 5
```

#### No Batching

To run the simulator with no batching, simply set the `MAX_BATCH_SIZE` to 1.

#### Static Batching

To run the simulator with static batching, simply set the `MAX_BATCH_SIZE` > 1 and choose a `BATCH_WAIT_TIMEOUT` based on the expected batch size and the average job duration.

#### Dynamic Batching

To run the simulator with dynamic batching, set the `DYNAMIC_BATCHING = True` with `MAX_BATCH_SIZE` > 1.
Dynamic batching is also called continuous batching.
Rebatching is performed when at least 1 job in the batch has finished. The benefit is that the early-finished jobs in a batch don't need to wait for the remaining jobs in the batch to finish.

### Evaluation

Configure the parameters in `util.py`; an example could be:

```
LLM_DATA_PATH = 'prediction/final/predictions_all.csv'  # multiple predictors
CLASSIFIERS = ['reg-l1', 'reg-mse', 'cls', 'multi-cls', 'multi-cls-l1', 'multi-cls-mse']
RESULTS_DIR = 'results/cleaned_datasets_no_batching_single_round/'
```

Run the evaluation script and plot the results on a barplot:

#### No Batching

Set these parameters in `util.py`:
- MODE = 'NO_BATCHING'
- MAX_BATCH_SIZE = 1
- DYNAMIC_BATCHING = False

```
python auto_eval.py
python auto_eval_barplot.py
```

#### Static Batching

Set these parameters in `util.py`:
- MODE = 'STATIC_BATCHING'
- MAX_BATCH_SIZE = 2
- DYNAMIC_BATCHING = False

```
python auto_eval.py
python auto_eval_barplot.py
```

#### Dynamic Batching

Set these parameters in `util.py`:
- MODE = 'DYNAMIC_BATCHING'
- MAX_BATCH_SIZE = 2
- DYNAMIC_BATCHING = True

```
python auto_eval.py
python auto_eval_barplot.py
```
