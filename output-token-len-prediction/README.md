# Proxy-model based Output Token Length Prediction

### Output Token Length Prediction

Training and evaluation dataset generation:

```
python preprocess_dataset.py [--FLAGS]
```

Training and evaluation of the output token length predictor:

```
python latency_prediction.py [--FLAGS]
```

#### Training Modes and Usage

Predictor supports four basic modes:
- Regression `--task_type 0`
- Binary Classification `--task_type 1`
- Multi-class Classification `--task_type 2`
- Multi-class Ordinal Classification `--task_type 3`
- Bi-class Ordinal Classification `--task_type 4`

For regression and ordinal classification, you can choose to use L1 loss or MSE loss during training:
- L1 loss `--l1_loss`
- MSE loss (simply no flag)

To enable multi-round support, add the `--multi_round` flag.

To limit the data size, add the ``

Example commands can be found in `output-token-len-prediction/script.sh`.

```
# data generation (10K samples)
python preprocess_dataset.py --task_type 0 --data_size 10

# predictor training (regression with MSE loss)
python latency_prediction.py --task_type 0 --data_size 10

# predictor training (regression with L1 loss)
python latency_prediction.py --task_type 0 --l1_loss --data_size 10
```
